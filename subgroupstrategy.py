"""
SubgroupStrategy is an interface required to be implemented.
The vertexes in different homogeneous networks (cluster) can overlaping, but 
the vertexes in each of them cannont be the same.
"""
from homogeneousnetwork import *
class SubgroupStrategy:
    def __init__(self):
        pass
    """
    # It should be implemented.
    # It returns {0: network0, 1: network1}
    """
    def do_subgroup(self, network):
        raise NotImplementedError("do_subgroup should be implemented.")
