"""
A Homogeneous Network has vertexes and pairs of weight.
The edges in it are undirected.
But the vertexes in Network are all in the same type.
"""
from network import *
import pickle
import operator
class HomogeneousNetwork(Network):
    def __init__(self):
        super(HomogeneousNetwork, self).__init__()
        """
        # {vertex_id1, vertex_id2}
        """
        self.vertex_id = set()
        """
        # weight:
        #     {(vertex_i, vertex_j): value}
        """
        self.weight = dict()
        self.vertex_number = 0

    def is_vertex_unique(self, vertex):
        try:
            if vertex in self.vertex_id:
                raise Exception('A vertex must be unique.')
        except Exception as e:
            print(type(e), str(e))
            raise

    def is_vertex_in_network(self, vertex):
        try:
            if vertex not in self.vertex_id:
                raise IndexError(vertex + ' is not in the Network ')
        except IndexError as e:
            print(type(e), str(e))

    def add_vertex(self, vertex):
        self.is_vertex_unique(vertex)
        self.vertex_id.add(vertex)
        self.vertex_number += 1

    def set_weight(self, vertex1, vertex2, value):
        self.is_vertex_in_network(vertex1)
        self.is_vertex_in_network(vertex2)
        """
        # weight:
        #     {(vertex_i, vertex_j): value}
        """
        if vertex1 > vertex2:
            vertex1, vertex2 = vertex2, vertex1

        """
        # edge weight is zero by default
        # remove it if the edge existed or do nothing
        """
        if value == 0:
            if (vertex1, vertex2) not in self.weight:
                pass
            else:
                self.weight.pop((vertex1, vertex2))
        else:
            self.weight[(vertex1, vertex2)] = value

    def get_weight(self, vertex1, vertex2):
        self.is_vertex_in_network(vertex1)
        self.is_vertex_in_network(vertex2)
        """
        # weight:
        #     {(vertex_i, vertex_j): value}
        """
        if vertex1 > vertex2:
            vertex1, vertex2 = vertex2, vertex1

        """
        # If there is no such vertex pair, then the weight of the edge is 0.
        """
        if (vertex1, vertex2) not in self.weight:
            return 0
        else:
            return self.weight[(vertex1, vertex2)]

    """
    # It creates a new network whose weight is 1 or 0.
    # If the w(i, j) >= threshold (op='>='),
    #   then w'(i, j) = 1
    # else w'(i, j) = 0 
    #
    # op can be '>=', '>', or '=='.
    # '<' will not be support, because number of edges may increase too fast.
    """
    def dichotomize(self, threshold, op):
        op_dict = {'>=': operator.ge, '>': operator.gt, '==': operator.eq}
        n = HomogeneousNetwork()
        for vertex in self.vertex_id:
            n.add_vertex(vertex)

        for edge in self.weight:
            vertex1, vertex2 = edge[0], edge[1]
            if op_dict[op](self.get_weight(vertex1, vertex2), threshold):
                n.set_weight(vertex1, vertex2, 1)
            else:
                n.set_weight(vertex1, vertex2, 0)
        return n

    """
    # subgroup returns dict of homogeneous networks
    #     {index: homogeneous network}
    # strategy is an interface
    """
    def subgroup(self, strategy):
        return strategy.do_subgroup(self)

    def save(self, path):
        with open(path, 'wb') as file:
            pickle.dump(self, file)

    @staticmethod
    def load(path):
        with open(path, 'rb') as file:
            return pickle.load(file)

if __name__ == '__main__':
    n = HomogeneousNetwork()

    n.add_vertex("1")
    n.add_vertex("2")
    n.add_vertex("3")
    n.add_vertex("4")
    n.add_vertex("5")

    print("vertex: ")
    for vertex in n.vertex_id:
        print(vertex)

    n.set_weight("1", "2", 0.4)
    n.set_weight("2", "3", 0.8)
    n.set_weight("5", "2", 75)
    n.set_weight("4", "3", 0)
    n.set_weight("3", "3", 0)
 
    print("weight: ")
    for edge in n.weight:
        print(edge, ": ", n.weight[edge])

    print('------------------------------')
    t = 0.5
    print('Dichotomizing Network')
    print('threshold: ', t)
    n2 = n.dichotomize(t, '>=')
    print('Vertex ID: ', n2.vertex_id)
    print('Weight: ', n2.weight)

    print('------------------------------')
    print("test save and load:")
    n.save("homogeneous_network_test")
    l = HomogeneousNetwork.load("homogeneous_network_test")
    print("print weight of loaded network: ")
    print(l.weight)
