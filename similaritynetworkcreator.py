"""
SimilarityNetworkCreator creates a homogeneous network based on a heterogeneous
network.
The vertexes in the homogeneous network are selected from one kind of vertex
in a heterogeneous network.
The other kind of vertex in the heterogeneous network is attibute of the vertex
in homogeneous network.
"""
from homogeneousnetwork import *
from heterogeneousnetwork import *
class SimilarityNetworkCreator:
    def __init__(self):
        pass

    """
    # parameters: heterogenous_network, vertex_type, vertex_attribute_type
    # return value: homogeneous_network
    """
    @staticmethod
    def create_similarity_network(network, t, at):
        raise NotImplementedError("create_similarity_network should be implemented." )
    

