"""
Shannon Index:
    - Sigma(from 1 to number of clusters, 
          p_i * log(base, p_i))

    p_i: vertexes_in_cluster_i / vertexes_in_all_cluster
    log: It can be differnet base, a user can specify base.
         The base is 2, 10, or e, normally.
"""
from diversitycalculator import *
from homogeneousnetwork import *
from math import *
class ShannonDiversityCalculator(DiversityCalculator):
    def __init__(self):
        pass

    """
    # parameters: dict of homogeneous_network
    #     {0: network1, 1: network2}
    # returns: a diversity value
    """
    @staticmethod
    def cal_diversity(clusters, base = e):
        vertex_count = 0
        for index in range(len(clusters)):
            vertex_count += clusters[index].vertex_number
        """
        # p = {0: p_0, 1: p_1}
        """
        p = dict()
        for index in range(len(clusters)):
            p[index] = clusters[index].vertex_number / vertex_count
        """
        # Shannon:
        """
        diversity = 0
        for index in range(len(clusters)):
            diversity -= p[index] * log(p[index], base)
        return diversity

if __name__ == '__main__':
    c0 = HomogeneousNetwork()
    c1 = HomogeneousNetwork()
    c2 = HomogeneousNetwork()
    c3 = HomogeneousNetwork()

    c0.add_vertex("v0")
    c0.add_vertex("v1")
    c0.add_vertex("v2")

    c1.add_vertex("v3")
    c1.add_vertex("v4")
    c1.add_vertex("v5")
    c1.add_vertex("v6")

    c2.add_vertex("v6")

    c3.add_vertex("v7")
    c3.add_vertex("v8")

    clusters = {0: c0, 1:c1, 2:c2, 3:c3}
    d_e = ShannonDiversityCalculator.cal_diversity(clusters)
    d_2 = ShannonDiversityCalculator.cal_diversity(clusters, 2)
    print(d_e)
    print(d_2)
