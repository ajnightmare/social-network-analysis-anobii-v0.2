"""
# There may be some NaN similarity in the network.
# A user should define it by setting the parameter.
"""
from similaritynetworkcreator import *
class LinModSimilarityNetworkCreator(SimilarityNetworkCreator):
    """
    # parameters: heterogenous_network, vertex_type, 
    #             vertex_attribute_type, undefined_exchange_value
    # return value: homogeneous_network
    """
    @staticmethod
    def create_similarity_network(network, t, at, und = float('nan')):
        similarity_network = HomogeneousNetwork()
        """
        # {"vertex1": set_of_attribute1}
        """
        vertex_attribute = dict()

        for vertex in network.vertex_id[t]:
            similarity_network.add_vertex(vertex)
            vertex_attribute[vertex] = set()

        """
        # if t < at
        #     (vertex1, vertex2) is t, at pair
        """
        if t < at:
            for edge in network.weight[(t,at)]:
                vertex, attribute = edge[0], edge[1]
                vertex_attribute[vertex].add(attribute)
        else:
            for edge in network.weight[(at,t)]:
                vertex, attribute = edge[1], edge[0]
                vertex_attribute[vertex].add(attribute)

        for vertex1 in network.vertex_id[t]:
            for vertex2 in network.vertex_id[t]:
                if vertex1 > vertex2:
                    continue
                """
                # LinMod:
                #     A intersection B - 1 / (min(A, B) - 1)
                #     if the denominator is 0, then similarity is 0!!!
                """
                a = vertex_attribute[vertex1]
                b = vertex_attribute[vertex2]

                """
                # undefined situation
                """
                if min(len(a), len(b)) - 1 == 0:
                    s = und
                else:
                    s = (len(a & b) - 1) / (min(len(a), len(b)) - 1)
                similarity_network.set_weight(vertex1, vertex2, s)
        
        return similarity_network

if __name__ == '__main__':
    n = HeterogeneousNetwork()

    n.add_vertex("a", "book")
    n.add_vertex("b", "book")
    n.add_vertex("c", "book")
    n.add_vertex("d", "book")

    n.add_vertex("1", "owner")
    n.add_vertex("2", "owner")
    n.add_vertex("3", "owner")

    n.set_weight("a", "book", "1", "owner", 1)
    n.set_weight("a", "book", "2", "owner", 1)
    n.set_weight("a", "book", "3", "owner", 1)

    n.set_weight("b", "book", "2", "owner", 1)
    n.set_weight("b", "book", "3", "owner", 1)

    n.set_weight("c", "book", "1", "owner", 1)
    n.set_weight("c", "book", "3", "owner", 1)

    n.set_weight("d", "book", "3", "owner", 1)

    sn = LinModSimilarityNetworkCreator. \
        create_similarity_network(n, "book", "owner")

    print("Heterogeneous Network: ")
    for edge in n.weight["book", "owner"]:
        print("    ", edge, ": ", n.weight["book", "owner"][edge])
    print("--------------------------------")
    print("Similarity Network: ")
    for edge in sn.weight:
        print("    ", edge, ": ", sn.weight[edge])
    print("---------------------------------")
