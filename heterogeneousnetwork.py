"""
A Heterogenous Network has vertexes and pairs of weight.
The edges in it are undirected.
But the vertexes in Network can be different type of instance.
For example, the vertex type can be book or owner or music.
"""
from network import *
import pickle
class HeterogeneousNetwork(Network):
    def __init__(self):
        super(HeterogeneousNetwork, self).__init__()
        """
        # {"book": vertex_id_set, "owner": vertex_id_set}
        """
        self.vertex_id = dict() 
        """
        # weight:
        #     {("book", "book"): book_book_weight, 
        #      ("book", "owner"): book_owner_weight}
        #
        #     book_owner_weight: 
        #         {(vertex_i, vertex_j): value}
        """
        self.weight = dict()
        """
        # {"book": book_num, "owner": owner_num}
        """
        self.vertex_number = dict()

    """
    # override and modify the signature of method
    """
    def is_vertex_unique(self, vertex, t):
        try:
            if vertex in self.vertex_id[t]:
                raise Exception('A vertex must be unique.')
        except Exception as e:
            print(type(e), str(e))
            raise

    def is_vertex_in_network(self, vertex, t):
        try:
            if vertex not in self.vertex_id[t]:
                raise IndexError(vertex + ' is not in the Network ')
        except IndexError as e:
            print(type(e), str(e))

    """
    # type is a string ("book", or "owner")
    """
    def add_vertex(self, vertex, t):
        if t not in self.vertex_id:
            self.vertex_id[t] = set()
            self.vertex_number[t] = 0
        else:
            self.is_vertex_unique(vertex, t)
        self.vertex_id[t].add(vertex)
        self.vertex_number[t] += 1

    def set_weight(self, vertex1, t1, vertex2, t2, value):
        self.is_vertex_in_network(vertex1, t1)
        self.is_vertex_in_network(vertex2, t2)
        """
        # weight:
        #     {("book", "book"): book_book_weight, 
        #      ("book", "owner"): book_owner_weight}
        #     type1 <= type2
        #
        #     book_owner_weight: 
        #         {(vertex_i, vertex_j): value}
        #
        #     if type1 == type2
        #         vertex_i <= vertex_j 
        """
        if t1 == t2:
            if vertex1 > vertex2:
                vertex1, vertex2 = vertex2, vertex1
        else:
            if t1 > t2:
                vertex1, vertex2 = vertex2, vertex1
                t1, t2 = t2, t1
        if (t1, t2) not in self.weight: 
            self.weight[(t1, t2)] = dict()

        """
        # edge weight is zero by default
        # remove it if the edge existed or do nothing
        """
        if value == 0:
            if (vertex1, vertex2) not in self.weight[(t1, t2)]:
                pass
            else:
                self.weight[(t1, t2)].pop((vertex1, vertex2))
        else:
            self.weight[(t1, t2)][(vertex1, vertex2)] = value 

    def get_weight(self, vertex1, t1, vertex2, t2):
        self.is_vertex_in_network(vertex1, t1)
        self.is_vertex_in_network(vertex2, t2)
        """
        # weight:
        #     {("book", "book"): book_book_weight, 
        #      ("book", "owner"): book_owner_weight}
        #     type1 <= type2
        #
        #     book_owner_weight: 
        #         {(vertex_i, vertex_j): value}
        #
        #     if type1 == type2
        #         vertex_i <= vertex_j 
        """
        if t1 == t2:
            if vertex1 > vertex2:
                vertex1, vertex2 = vertex2, vertex1
        else:
            if t1 > t2:
                vertex1, vertex2 = vertex2, vertex1
                t1, t2 = t2, t1

        """
        # If type pair or vertex pair is not in dict,
        # it is considered as no edge and has 0 weight. 
        """
        if (t1, t2) not in self.weight: 
            return 0
        else:
            if (vertex1, vertex2) not in self.weight[(t1, t2)]:
                return 0
            else:
                return self.weight[(t1, t2)][(vertex1, vertex2)]

    def save(self, path):
        with open(path, 'wb') as file:
            pickle.dump(self, file)

    @staticmethod
    def load(path):
        with open(path, 'rb') as file:
            return pickle.load(file)

if __name__ == '__main__':
    n = HeterogeneousNetwork()
    """
    # vertex uniqueness test:
    # remove the comment notation
    """
    # n.add_vertex("a", "book")


    n.add_vertex("a", "book")
    n.add_vertex("b", "book")
    n.add_vertex("c", "book")
    n.add_vertex("1", "owner")
    n.add_vertex("2", "owner")
    n.add_vertex("3", "owner")

    n.set_weight("a", "book", "1", "owner", 2.3)
    n.set_weight("1", "owner", "b", "book", 1.7)
    n.set_weight("3", "owner", "1", "owner", 1.6)
    n.set_weight("2", "owner", "3", "owner", 0)
    n.set_weight("2", "owner", "2", "owner", 8)

    print(n.get_weight("c", "book", "3", "owner"))
    print(n.get_weight("1", "owner", "a", "book"))

    print("vertex number: ")
    for t in n.vertex_number:
        print("    ", t, ": ", n.vertex_number[t])

    print("weight: ")
    for element in n.weight:
        print(element)
        print("    ", n.weight[element])

    print("test save and load:")
    n.save("heterogeneous_network_test")
    l = HeterogeneousNetwork.load("heterogeneous_network_test")
    print("print weight of loaded network: ")
    print(l.weight)
