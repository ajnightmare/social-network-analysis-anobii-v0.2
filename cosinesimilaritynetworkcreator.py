from similaritynetworkcreator import *
from math import sqrt
class CosineSimilarityNetworkCreator(SimilarityNetworkCreator):
    """
    # parameters: heterogenous_network, vertex_type, vertex_attribute_type
    # return value: homogeneous_network
    """
    @staticmethod
    def create_similarity_network(network, t, at):
        similarity_network = HomogeneousNetwork()
        """
        # {"vertex1": set_of_attribute1}
        """
        vertex_attribute = dict()

        for vertex in network.vertex_id[t]:
            similarity_network.add_vertex(vertex)
            vertex_attribute[vertex] = set()

        """
        # if t < at
        #     (vertex1, vertex2) is t, at pair
        """
        if t < at:
            for edge in network.weight[(t,at)]:
                vertex, attribute = edge[0], edge[1]
                vertex_attribute[vertex].add(attribute)
        else:
            for edge in network.weight[(at,t)]:
                vertex, attribute = edge[1], edge[0]
                vertex_attribute[vertex].add(attribute)

        for vertex1 in network.vertex_id[t]:
            for vertex2 in network.vertex_id[t]:
                if vertex1 > vertex2:
                    continue
                """
                # cosine:
                #     A dot B / (sqrt(A dot A) * sqrt(B dot B)))
                """
                a = vertex_attribute[vertex1]
                b = vertex_attribute[vertex2]
                a_and_b = a & b
                s = len(a_and_b) / (sqrt(len(a) * len(b)))
                similarity_network.set_weight(vertex1, vertex2, s)
        
        return similarity_network

if __name__ == '__main__':
    n = HeterogeneousNetwork()

    n.add_vertex("a", "book")
    n.add_vertex("b", "book")
    n.add_vertex("c", "book")
    n.add_vertex("d", "book")

    n.add_vertex("1", "owner")
    n.add_vertex("2", "owner")
    n.add_vertex("3", "owner")

    n.set_weight("a", "book", "1", "owner", 1)
    n.set_weight("a", "book", "2", "owner", 1)
    n.set_weight("a", "book", "3", "owner", 1)

    n.set_weight("b", "book", "2", "owner", 1)
    n.set_weight("b", "book", "3", "owner", 1)

    n.set_weight("c", "book", "1", "owner", 1)
    n.set_weight("c", "book", "3", "owner", 1)

    n.set_weight("d", "book", "3", "owner", 1)

    sn = CosineSimilarityNetworkCreator. \
        create_similarity_network(n, "book", "owner")

    print("Heterogeneous Network: ")
    for edge in n.weight["book", "owner"]:
        print("    ", edge, ": ", n.weight["book", "owner"][edge])
    print("--------------------------------")
    print("Similarity Network: ")
    for edge in sn.weight:
        print("    ", edge, ": ", sn.weight[edge])
    print("---------------------------------")
