"""
A DiversityCalulator calculates diversity of a network, 
and it see network as clusters.

The clusters may or may not have same vertexes.
But, the vertexes in a cluster are not the same.

Diversity Inder is based on:
    https://en.wikipedia.org/wiki/Diversity_index
"""

class DiversityCalculator:
    def __init__(self):
        pass

    """
    # parameters: dict of homogeneous_network
    #     {0: network1, 1: network2}
    # returns: a diversity value
    """
    @staticmethod
    def cal_diversity(clusters):
        raise NotImplementedError("cal_diversity should be implemented." )
