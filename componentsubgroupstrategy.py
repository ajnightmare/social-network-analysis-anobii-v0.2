"""
Component Subgroup is implemented by DFS.
Isolates are handle separtedly, because we assume there are few edges.

Each component is a network, and the edge of it
    is Not DFS path
    but edge with weight of the subgouped network.
"""
from subgroupstrategy import *
class ComponentSubgroupStrategy(SubgroupStrategy):
    def __init__(self):
        pass
    """
    # Network are subgrouped to components (see a component as a network)
    # Components are found by DFS algorithm
    # Edges are assumed to be sparse.
    """
    def do_subgroup(self, network):
        components = dict()
        """
        # If the network has no vertexes, a null dict will be return
        """
        if not network.vertex_id:
            return components
        """
        # Isolates will be dealt first, and then
        # the rest of vertexes are dealt by DFS
        """
        index = 0
        vertex_list = list(network.vertex_id)
        vertex_list.sort()
        for vertex in vertex_list:
            """
            # An isolate is a single component
            """
            is_isolate = True
            for edge in network.weight:
                if vertex in edge:
                    is_isolate = False
            if is_isolate:
                component = HomogeneousNetwork()
                component.add_vertex(vertex)
                vertex_list.remove(vertex)
                component.id = index
                components[index] = component
                index += 1
        """
        # Vertexes in List are all non-isolate now.
        # DFS algorithm is based on hil's DFS ppt
        # edge (3,2) is represented as (2,3)
        """
        """
        # construct a dict for very vertex
        # If it is visited, then value will be True.
        """
        visited = dict()
        for vertex in vertex_list:
            visited[vertex] = False
        for vertex in vertex_list:
            def visit(vertex, component):
                visited[vertex] = True
                for edge in network.weight:
                    """
                    # Every edge is store as (a, b), where a <= b, 
                    # so vertexes are in topological order such that
                    # (b, a) edges aren't needed to concerned.
                    """
                    source, neighbor = edge
                    if vertex == source:
                        # add all edge into the component
                        if not visited[neighbor]:
                            component.add_vertex(neighbor)
                            weight = network.get_weight(vertex, neighbor)
                            component.set_weight(vertex, neighbor, weight)
                            visit(neighbor, component)
                        else:
                            weight = network.get_weight(vertex, neighbor)
                            component.set_weight(vertex, neighbor, weight)
            """
            # subroutine end
            """

            if not visited[vertex]:
                # a new component found
                component = HomogeneousNetwork()
                component.add_vertex(vertex)
                visit(vertex, component)
                component.id = index
                components[index] = component
                index += 1

        return components

if __name__ == '__main__': 
    n = HomogeneousNetwork()
    for vertex in range(8):
        n.add_vertex(str(vertex))
    # a triangle
    n.set_weight('0', '1', 1)
    n.set_weight('1', '2', 1)
    n.set_weight('2', '1', 1)

    # a rectangle with additional edge in it
    n.set_weight('3', '4', 1)
    n.set_weight('4', '5', 1)
    n.set_weight('5', '6', 1)
    n.set_weight('6', '3', 1)
    n.set_weight('3', '5', 1)

    # a single point '7'  without any edge

    strategy = ComponentSubgroupStrategy()
    subgroup = strategy.do_subgroup(n)
    for index in subgroup:
        print('Component: ', index)
        print("    vertex: ", subgroup[index].vertex_id)
        print("    weight: ", subgroup[index].weight)
