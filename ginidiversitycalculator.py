"""
Gini Index:
    1 - 1 / N * Sigma(from 1 to N, 
          p_i_minus_1' + p_i')

    N: number_of_clusters
    p_i: vertexes_in_cluster_i / vertexes_in_all_cluster
    p_0 is defined as 0.
    p_i': p_i sorted from low to high
"""
from diversitycalculator import *
from homogeneousnetwork import *
class ShannonDiversityCalculator(DiversityCalculator):
    def __init__(self):
        pass

    """
    # parameters: dict of homogeneous_network
    #     {0: network1, 1: network2}
    # returns: a diversity value
    """
    @staticmethod
    def cal_diversity(clusters):
        n = len(clusters)
        vertex_count = 0
        for index in range(n):
            vertex_count += clusters[index].vertex_number
        """
        # p = {0: p_0, 1: p_1}
        """
        p = list()
        for index in range(n):
            p.append(clusters[index].vertex_number / vertex_count)
        p.sort()
        print(p, n)
        """
        # Gini:
        """
        sigma = 0
        for index in range(n):
            if index == 0:
                sigma += p[index]
            else:
                sigma += p[index - 1] + p[index]
        diversity = 1 - sigma / n
        return diversity

if __name__ == '__main__':
    c0 = HomogeneousNetwork()
    c1 = HomogeneousNetwork()
    c2 = HomogeneousNetwork()
    c3 = HomogeneousNetwork()

    c0.add_vertex("v0")
    c0.add_vertex("v1")
    c0.add_vertex("v2")

    c1.add_vertex("v3")
    c1.add_vertex("v4")
    c1.add_vertex("v5")
    c1.add_vertex("v6")

    c2.add_vertex("v6")

    c3.add_vertex("v7")
    c3.add_vertex("v8")

    clusters = {0: c0, 1:c1, 2:c2, 3:c3}
    d = ShannonDiversityCalculator.cal_diversity(clusters)
    print(d)
