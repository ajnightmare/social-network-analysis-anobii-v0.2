"""
RawDataHandler class is used to wrap book-owner data from mysql into 
a heterogeneous network.

"jobs.csv" records the interviewed owners.
Owner "012208e616ab666cf2" in "jobs.csv" is "dostaevsky" in "owner.csv".

The owners with 76 ("leave") and 82 ("014f19e3bd2afae513") secondary_id
are just for test uses.
They must be excluded from overall bookcollection list and owners'
bookcollection list.
"""
import csv
from heterogeneousnetwork import *
class RawDataHandler:
    def __init__(self):
        pass

    @staticmethod
    def create_overall_network():
        """
        # owner.csv formt:
        #   id, book_id, owner_id
        #   where id doesn't means anything, just indexes
        """
        with open('raw_data/owner.csv', newline='') as ownerfile:
            """
            # Add all books but the books only owned by
            # owner "leave" and owner "014f19e3bd2afae513"
            # Add all book vertex and owner vertex and their edges.
            """ 
            n = HeterogeneousNetwork()
            reader = csv.reader(ownerfile, delimiter=',')
            for row in reader:
                book_id = row[1]
                owner_id = row[2]
                if owner_id == "leave" or owner_id == "014f19e3bd2afae513":
                    continue
                if "book" not in n.vertex_id or \
                   book_id not in n.vertex_id["book"]:
                    n.add_vertex(book_id, "book")
                if  "owner" not in n.vertex_id or \
                    owner_id not in n.vertex_id["owner"]:
                    n.add_vertex(owner_id, "owner")
                n.set_weight(book_id, "book", owner_id, "owner", 1)
            return n 

    @staticmethod
    def create_owner_networks():
        """
        # jobs.csv format:
        #    secondary_id, email, status, timestamp, primary_id

        #    We only collect owners whose status is 8.
        #    The owners with 76 and 82 secondary_id are just for test uses.
        """
        owner_id_list = []
        with open('raw_data/jobs.csv', newline='') as ownerfile:
            reader = csv.reader(ownerfile, delimiter=',')
            for row in reader:
                primary_id = row[4]
                secondary_id = row[0]
                status = row[2]
                if (status == '8' and
                    secondary_id != '76' and
                    secondary_id != '82'):
                    owner_id_list.append(primary_id)
            """
            # owner "012208e616ab666cf2" in "jobs.csv" is
            #       "dostaevsky" in "owner.csv"
            """
            owner_id_list.remove("012208e616ab666cf2")
            owner_id_list.append("dostaevsky")

        networks = dict()
        for owner in owner_id_list:
            networks[owner] = HeterogeneousNetwork()
            networks[owner].id = owner
            networks[owner].add_vertex(owner, "owner")
        """
        # owner.csv formt:
        #   id, book_id, owner_id
        #   where id doesn't means anything, just indexes
        """
        with open('raw_data/owner.csv', newline='') as ownerfile:
            """
            # Add all books but the books only owned by
            # owner "leave" and owner "014f19e3bd2afae513"
            # Add all book vertex and owner vertex and their edges.
            """ 
            reader = csv.reader(ownerfile, delimiter=',')
            for row in reader:
                book_id = row[1]
                owner_id = row[2]
                if owner_id == "leave" or owner_id == "014f19e3bd2afae513":
                    continue
                if owner_id in networks:
                    networks[owner_id].add_vertex(book_id, "book")
                    networks[owner_id].set_weight(book_id, "book",\
                                                  owner_id, "owner", 1)

        return networks

if __name__ == '__main__':
    print('RawDataHandler') 
    print("----------------------------")
    overall_n = RawDataHandler.create_overall_network()
    owner_ns = RawDataHandler.create_owner_networks()
    print('Number of Owners: ', overall_n.vertex_number["owner"])
    print('Number of Books owned by Owners: ', overall_n.vertex_number["book"])
    print("----------------------------")
