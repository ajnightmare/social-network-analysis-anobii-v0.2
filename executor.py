from rawdatahandler import *
from cosinesimilaritynetworkcreator import *
import os
from multiprocessing import Pool
def cosine_outer(network, t, at):
    return Executor.cosine(network, t, at)
class Executor:
    def __init__(self):
        pass
    @staticmethod
    def cosine(n, t, at):
        path = "anobii_network/similarity_network/cosine.hon"
        if os.path.isfile(path + ".ok"):
            print("---Loading Cosine Similarity HomogeneousNetwork---")
            sn = HomogeneousNetwork.load(path)
        else:
            print("---Calculating Cosine Similarity HomogeneousNetwork---")
            sn = CosineSimilarityNetworkCreator. \
                create_similarity_network(n, "book", "owner")
            sn.save(path)
            file = open(path + ".ok", "w")
        return sn 
    
    def main(self):
        """
        # overall heterogeneous network
        # owner heterogeneous networks
        """
        path = "anobii_network/overall/overall.hn"
        if os.path.isfile(path + ".ok"):
            print("---Loading Overall HeterogeneousNetwork---")
            overall_network = HeterogeneousNetwork.load(path)
        else:
            print("---Calculating Overall HeterogeneousNetwork---")
            overall_network = RawDataHandler.create_overall_network()
            overall_network.save(path)
            file = open(path + ".ok", "w")

        path = "anobii_network/owners/"
        if os.path.isfile(path + "dir.ok"):
            print("---Loading Owners' HeterogeneousNetwork---")
            owner_networks = dict()
            for filename in os.listdir(path):
                # we don't load dir.ok file into network
                if filename != "dir.ok":
                    """
                    # owner.hn to owner
                    """
                    owner = filename[:-3]
                    owner_networks[owner] = HeterogeneousNetwork. \
                        load(path + filename)
        else:
            print("---Calculating Owners' HeterogeneousNetwork---")
            owner_networks = RawDataHandler.create_owner_networks()
            for owner in owner_networks:
                owner_networks[owner].save(path + owner + ".hn")
            file = open(path + "dir.ok", "w")

        """
        # book-book all pair similarity
        # similarity network methods:
        #     jaccard
        #     lin
        #     lin_mod
        #     cosine
        #     correlation
        #
        # "book", "owner": vertex, attribute
        """
        with Pool(processes = 5) as pool:
            w_cosine = pool.apply_async(\
                cosine_outer, (overall_network, "book", "owner") )
            cosine_network = w_cosine.get() 

if __name__ == '__main__':
    e = Executor()
    e.main()
