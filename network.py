"""
Network is a abstract class.
vertex_number must be handle dynamically to accumulate large network.
"""
class Network:
    def __init__(self):
        self.id = ""

    def is_vertex_unique():
        raise NotImplementedError( "is_vertex_unique() should be implemented." )
