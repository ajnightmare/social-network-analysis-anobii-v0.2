"""
# There may be some NaN similarity in the network.
# A user should define it by setting the parameter.
"""
from similaritynetworkcreator import *
from math import sqrt
class CorrelationSimilarityNetworkCreator(SimilarityNetworkCreator):
    """
    # parameters: heterogenous_network, vertex_type, 
    #             vertex_attribute_type, undefined_exchange_value
    # return value: homogeneous_network
    """
    @staticmethod
    def create_similarity_network(network, t, at, und = float('nan')):
        similarity_network = HomogeneousNetwork()
        """
        # {"vertex1": set_of_attribute1}
        """
        vertex_attribute = dict()
        all_attribute = set()

        for vertex in network.vertex_id[t]:
            similarity_network.add_vertex(vertex)
            vertex_attribute[vertex] = set()

        """
        # if t < at
        #     (vertex1, vertex2) is t, at pair
        """
        if t < at:
            for edge in network.weight[(t,at)]:
                vertex, attribute = edge[0], edge[1]
                vertex_attribute[vertex].add(attribute)
                all_attribute.add(attribute)
        else:
            for edge in network.weight[(at,t)]:
                vertex, attribute = edge[1], edge[0]
                vertex_attribute[vertex].add(attribute)
                all_attribute.add(attribute)

        """
        # Correlation:
        #     (A - mean(A) dot (B - mean(B)) / 
        #     (||A - mean(A)|| * ||B - mean(B)||)
        #
        # attribute_number: all attribute vertex number in network
        #
        # a: 0 0 1 0
        # b: 1 1 1 0
        # attribute_number = 4
        # mean_a = 1 / 4
        # mean_b = 3 / 4
        """
        at_num = network.vertex_number[at]
        print("at_num: ", at_num)
        for vertex1 in network.vertex_id[t]:
            for vertex2 in network.vertex_id[t]:
                if vertex1 > vertex2:
                    continue
                a = vertex_attribute[vertex1]
                b = vertex_attribute[vertex2]
                mean_a = len(a) / at_num
                mean_b = len(b) / at_num
                print("a: ", a, "len(a): ", len(a))
                print("b: ", b, "len(b): ", len(b))

                # a - mean(a) dot (b - mean(b))
                numerator = \
                    (1 - mean_a) * (1 - mean_b) * len(a & b) + \
                    (0 - mean_a) * (0 - mean_b) * (at_num - len(a | b)) + \
                    (1 - mean_a) * (0 - mean_b) * len(a - b) + \
                    (0 - mean_a) * (1 - mean_b) * len(b - a) 

                # ||a - mean(a)||^2
                d_left = \
                    (1 - mean_a) ** 2 * len(a) + \
                    (0 - mean_a) ** 2 * (at_num - len(a))
 
                # ||b - mean(b)||^2
                d_right = \
                    (1 - mean_b) ** 2 * len(b) + \
                    (0 - mean_b) ** 2 * (at_num - len(b))

                print("d_left: ", d_left)
                print("d_right: ", d_right)
                """
                # If a or b has zero variance then r is not defined.
                """
                if d_left == 0 or d_right == 0:
                    s = und
                else:
                    s = numerator / sqrt(d_left * d_right)
                similarity_network.set_weight(vertex1, vertex2, s)
        
        return similarity_network

if __name__ == '__main__':
    n = HeterogeneousNetwork()

    n.add_vertex("a", "book")
    n.add_vertex("b", "book")
    n.add_vertex("c", "book")
    n.add_vertex("d", "book")

    n.add_vertex("1", "owner")
    n.add_vertex("2", "owner")
    n.add_vertex("3", "owner")

    n.set_weight("a", "book", "1", "owner", 1)
    n.set_weight("a", "book", "2", "owner", 1)
    n.set_weight("a", "book", "3", "owner", 1)

    n.set_weight("b", "book", "2", "owner", 1)
    n.set_weight("b", "book", "3", "owner", 1)

    n.set_weight("c", "book", "1", "owner", 1)
    n.set_weight("c", "book", "3", "owner", 1)

    n.set_weight("d", "book", "3", "owner", 1)

    sn = CorrelationSimilarityNetworkCreator. \
        create_similarity_network(n, "book", "owner")

    print("Heterogeneous Network: ")
    for edge in n.weight["book", "owner"]:
        print("    ", edge, ": ", n.weight["book", "owner"][edge])
    print("--------------------------------")
    print("Similarity Network: ")
    for edge in sn.weight:
        print("    ", edge, ": ", sn.weight[edge])
    print("---------------------------------")
